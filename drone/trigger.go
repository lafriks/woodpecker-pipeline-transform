package drone

import (
	"errors"

	transform "codeberg.org/lafriks/woodpecker-pipeline-transform"
)

func TriggerToGlobalWhen(trigger *Trigger) (*transform.When, error) {
	if trigger == nil {
		return nil, nil
	}

	if len(trigger.Status) != 0 {
		return nil, errors.New("unsupported global trigger condition \"status\"")
	}

	when := &transform.When{
		Event:  &transform.Conditions{Conditions: trigger.Event},
		Repo:   trigger.Repositories.Transform(),
		Branch: trigger.Branch.Transform(),
		Cron:   trigger.Cron.Transform(),
		Ref:    trigger.Refs.Transform(),
	}

	if len(trigger.Event) != 0 {
		when.Event = &transform.Conditions{
			Conditions: trigger.Event,
		}
	}

	// unsupported trigger conditions
	if !trigger.Action.IsEmpty() ||
		!trigger.Target.IsEmpty() {
		return nil, errors.New("unsupported trigger condition")
	}

	return when, nil
}
