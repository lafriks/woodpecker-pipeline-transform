// Copyright 2022 Lauris BH. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.

package drone

import (
	transform "codeberg.org/lafriks/woodpecker-pipeline-transform"
	"codeberg.org/lafriks/woodpecker-pipeline-transform/core"
)

type Platform struct {
	OS      string `yaml:"os"`
	Arch    string `yaml:"arch"`
	Version string `yaml:"version"`
}

type Workspace struct {
	Path string `yaml:"path"`
}

type Clone struct {
	Disable bool `yaml:"disable"`
	Depth   *int `yaml:"depth"`
}

type VolumeHost struct {
	Path string `yaml:"path"`
}

type VolumeTemp struct {
	Medium string `yaml:"medium"`
}

type Volume struct {
	Name string      `yaml:"name"`
	Host *VolumeHost `yaml:"host"`
	Temp *VolumeTemp `yaml:"temp"`
}

type VolumeRef struct {
	Name string `yaml:"name"`
	Path string `yaml:"path"`
}

type Step struct {
	Name        string             `yaml:"name"`
	Image       string             `yaml:"image"`
	Pull        string             `yaml:"pull"`
	Environment core.MapOrEnvArray `yaml:"environment"`
	Settings    transform.Settings `yaml:"settings"`
	Commands    []string           `yaml:"commands"`
	DependsOn   []string           `yaml:"depends_on"`
	Detach      bool               `yaml:"detach"`
	Privileged  bool               `yaml:"privileged"`
	Failure     string             `yaml:"failure"`
	Volumes     []VolumeRef        `yaml:"volumes"`
	When        *When              `yaml:"when"`
}

type Service struct {
	Name        string             `yaml:"name"`
	Image       string             `yaml:"image"`
	Pull        string             `yaml:"pull"`
	Environment core.MapOrEnvArray `yaml:"environment"`
	Commands    []string           `yaml:"commands"`
	Entrypoint  []string           `yaml:"entrypoint"`
	Privileged  bool               `yaml:"privileged"`
	Volumes     []VolumeRef        `yaml:"volumes"`
	WorkingDir  string             `yaml:"working_dir"`
}

type Pipeline struct {
	Kind      string            `yaml:"kind"`
	Type      string            `yaml:"type"`
	Name      string            `yaml:"name"`
	Platform  *Platform         `yaml:"platform"`
	Workspace *Workspace        `yaml:"workspace"`
	Node      map[string]string `yaml:"node"`
	Clone     *Clone            `yaml:"clone"`
	Services  []*Service        `yaml:"services"`
	Steps     []*Step           `yaml:"steps"`
	Trigger   *Trigger          `yaml:"trigger"`
	DependsOn []string          `yaml:"depends_on"`
	Volumes   []*Volume         `yaml:"volumes"`
}
