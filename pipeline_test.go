// Copyright 2022 Lauris BH. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.

package transform_test

import (
	"testing"

	transform "codeberg.org/lafriks/woodpecker-pipeline-transform"

	"github.com/goccy/go-yaml"
	"github.com/stretchr/testify/assert"
)

func TestPipelineUnmarshal(t *testing.T) {
	p := transform.Pipeline{
		Steps: transform.Steps{
			&transform.Step{
				Name:     "step1",
				Image:    "alpine:latest",
				Commands: []string{"echo Step 1", "echo $${CI_JOB_ID}"},
				When: &transform.When{
					Event: &transform.Conditions{
						Conditions: []string{"push", "pull_request"},
					},
					Branch: &transform.Conditions{
						Conditions: []string{"main"},
					},
				},
			},
			&transform.Step{
				Name:  "step2",
				Image: "alpine:latest",
			},
		},
	}

	buf, err := yaml.MarshalWithOptions(&p, yaml.IndentSequence(true))
	assert.NoError(t, err)
	assert.Equal(t, `steps:
  - name: step1
    image: alpine:latest
    commands:
      - echo Step 1
      - echo $${CI_JOB_ID}
    when:
      branch: main
      event:
        - push
        - pull_request
  - name: step2
    image: alpine:latest
`, string(buf))
}
