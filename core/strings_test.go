// Copyright 2022 Lauris BH. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.

package core

import (
	"testing"

	"github.com/goccy/go-yaml"
	"github.com/stretchr/testify/assert"
)

func TestSingleValueMarshal(t *testing.T) {
	v := &struct {
		Values Strings `yaml:"values"`
	}{
		Values: Strings{"test"},
	}
	buf, err := yaml.Marshal(v)
	assert.NoError(t, err)
	assert.Equal(t, "values: test\n", string(buf))
}

func TestMultipleValuesMarshal(t *testing.T) {
	v := &struct {
		Values Strings `yaml:"values"`
	}{
		Values: Strings{"val1", "val2"},
	}
	buf, err := yaml.Marshal(v)
	assert.NoError(t, err)
	assert.Equal(t, "values:\n- val1\n- val2\n", string(buf))
}

func TestSingleValueUnmarshal(t *testing.T) {
	v := &struct {
		Values Strings `yaml:"values"`
	}{}
	err := yaml.Unmarshal([]byte("values: test"), v)
	assert.NoError(t, err)
	assert.Equal(t, Strings{"test"}, v.Values)
}

func TestMultipleValueUnmarshal(t *testing.T) {
	v := &struct {
		Values Strings `yaml:"values"`
	}{}
	err := yaml.Unmarshal([]byte("values:\n- val1\n- val2"), v)
	assert.NoError(t, err)
	assert.Equal(t, Strings{"val1", "val2"}, v.Values)
}
