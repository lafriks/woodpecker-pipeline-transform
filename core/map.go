// Copyright 2022 Lauris BH. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.

package core

import (
	"fmt"
	"strings"
)

type ValueOrSecret struct {
	Value  string
	Secret string
}

// MapOrEnvArray represents a map or an array of strings as environment variables.
type MapOrEnvArray map[string]ValueOrSecret

// UnmarshalYAML implements the Unmarshaler interface.
func (m *MapOrEnvArray) UnmarshalYAML(unmarshal func(interface{}) error) error {
	mv := make(MapOrEnvArray)
	t := make(map[string]interface{})
	if err := unmarshal(&t); err == nil {
		for k, v := range t {
			switch vv := v.(type) {
			case map[string]interface{}:
				if val, ok := vv["from_secret"]; ok {
					mv[k] = ValueOrSecret{Secret: fmt.Sprintf("%v", val)}
				}
			default:
				mv[k] = ValueOrSecret{Value: fmt.Sprintf("%v", vv)}
			}
		}
		*m = mv
		return nil
	}
	arr := make([]string, 0)
	if err := unmarshal(&arr); err != nil {
		return err
	}
	for _, s := range arr {
		k, v, ok := strings.Cut(s, "=")
		if !ok {
			continue
		}
		mv[k] = ValueOrSecret{Value: v}
	}
	*m = mv
	return nil
}

// MarshalYAML implements the Marshaler interface.
func (m MapOrEnvArray) MarshalYAML() (interface{}, error) {
	arr := make([]string, 0)
	for k, v := range m {
		arr = append(arr, k+"="+v.Value)
	}
	return arr, nil
}
