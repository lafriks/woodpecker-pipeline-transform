// Copyright 2022 Lauris BH. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.

package core

import (
	"testing"

	"github.com/goccy/go-yaml"
	"github.com/stretchr/testify/assert"
)

func TestMapUnmarshal(t *testing.T) {
	v := &struct {
		Environment MapOrEnvArray `yaml:"environment"`
	}{}
	err := yaml.Unmarshal([]byte("environment:\n  FOO:\n    from_secret: bar\n  BAZ: 2\n"), v)
	assert.NoError(t, err)
	assert.Equal(t, MapOrEnvArray{
		"FOO": ValueOrSecret{Secret: "bar"},
		"BAZ": ValueOrSecret{Value: "2"},
	}, v.Environment)
}

func TestArrayUnmarshal(t *testing.T) {
	v := &struct {
		Environment MapOrEnvArray `yaml:"environment"`
	}{}
	err := yaml.Unmarshal([]byte("environment:\n- FOO=bar\n- BAZ=qux\n"), v)
	assert.NoError(t, err)
	assert.Equal(t, MapOrEnvArray{
		"FOO": ValueOrSecret{Value: "bar"},
		"BAZ": ValueOrSecret{Value: "qux"},
	}, v.Environment)
}
