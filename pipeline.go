// Copyright 2022 Lauris BH. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.

package transform

import (
	"errors"

	"github.com/goccy/go-yaml"
)

var ErrUnsupported = errors.New("unsupported pipeline")

type Step struct {
	Name        string   `yaml:"name"`
	Image       string   `yaml:"image,omitempty"       json:"image,omitempty"`
	Pull        bool     `yaml:"pull,omitempty"        json:"pull,omitempty"`
	Environment []string `yaml:"environment,omitempty" json:"environment,omitempty"`
	Secrets     Secrets  `yaml:"secrets,omitempty"     json:"secrets,omitempty"`
	Commands    []string `yaml:"commands,omitempty"    json:"commands,omitempty"`
	Settings    Settings `yaml:"settings,omitempty"    json:"settings,omitempty"`
	Detach      bool     `yaml:"detach,omitempty"      json:"detach,omitempty"`
	Privileged  bool     `yaml:"privileged,omitempty"  json:"privileged,omitempty"`
	Volumes     []string `yaml:"volumes,omitempty"     json:"volumes,omitempty"`
	When        *When    `yaml:"when,omitempty"        json:"when,omitempty"`
}

type Steps []*Step

type Service struct {
	Name        string   `yaml:"-"                     json:"-"`
	Image       string   `yaml:"image"                 json:"image"`
	Pull        bool     `yaml:"pull,omitempty"        json:"pull,omitempty"`
	Environment []string `yaml:"environment,omitempty" json:"environment,omitempty"`
	Secrets     Secrets  `yaml:"secrets,omitempty"     json:"secrets,omitempty"`
	Commands    []string `yaml:"commands,omitempty"    json:"commands,omitempty"`
	Settings    Settings `yaml:"settings,omitempty"    json:"settings,omitempty"`
	Privileged  bool     `yaml:"privileged,omitempty"  json:"privileged,omitempty"`
	Volumes     []string `yaml:"volumes,omitempty"     json:"volumes,omitempty"`
	When        *When    `yaml:"when,omitempty"        json:"when,omitempty"`
}

type Services []*Service

func (s Services) MarshalYAML() (interface{}, error) {
	v := make(yaml.MapSlice, len(s))
	for i, step := range s {
		v[i] = yaml.MapItem{
			Key:   step.Name,
			Value: step,
		}
	}
	return v, nil
}

type Workspace struct {
	Base string `yaml:"base" json:"base"`
	Path string `yaml:"path" json:"path"`
}

type CloneStep struct {
	Image    string   `yaml:"image,omitempty"    json:"image,omitempty"`
	Settings Settings `yaml:"settings,omitempty" json:"settings,omitempty"`
}

type Clone struct {
	Git *CloneStep `yaml:"git,omitempty" json:"git,omitempty"`
	Hg  *CloneStep `yaml:"hg,omitempty"  json:"hg,omitempty"`
}

type Pipeline struct {
	Name      string            `yaml:"-"                     json:"-"`
	Platform  string            `yaml:"platform,omitempty"    json:"platform,omitempty"`
	Labels    map[string]string `yaml:"labels,omitempty"      json:"labels,omitempty"`
	RunsOn    []string          `yaml:"runs_on,omitempty"     json:"runs_on,omitempty"`
	DependsOn []string          `yaml:"depends_on,omitempty"  json:"depends_on,omitempty"`
	Services  Services          `yaml:"services,omitempty"    json:"services,omitempty"`
	Workspace *Workspace        `yaml:"workspace,omitempty"   json:"workspace,omitempty"`
	SkipClone bool              `yaml:"skip_clone,omitempty"  json:"skip_clone,omitempty"`
	Clone     *Clone            `yaml:"clone,omitempty"       json:"clone,omitempty"`
	Steps     Steps             `yaml:"steps"                 json:"steps"`
	When      *When             `yaml:"when,omitempty"        json:"when,omitempty"`
}
