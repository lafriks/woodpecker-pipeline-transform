// Copyright 2022 Lauris BH. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.

package transform

type Source struct {
	Name    string
	Content []byte
}

type Transformer interface {
	Transform([]*Source) ([]*Pipeline, error)
	PostProcess([]byte) ([]byte, error)
}
