// Copyright 2022 Lauris BH. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.

package transform

import (
	"bufio"
	"bytes"
	"strings"
)

// FormatWoodpeckerYAML add newlines so that the pipeline YAML is easier to read.
func FormatWoodpeckerYAML(data []byte) ([]byte, error) {
	scanner := bufio.NewScanner(bytes.NewReader(data))
	out := bytes.NewBuffer(nil)
	var isPipeline bool
	var i int
	for scanner.Scan() {
		line := scanner.Text()
		if line == "steps:" || line == "pipeline:" || line == "services:" {
			isPipeline = true
			if i > 0 {
				out.WriteByte('\n')
			}
			i = 0
		} else if isPipeline && len(line) > 0 && line[0] != ' ' && line[0] != '\t' && line[0] != '-' {
			isPipeline = false
			out.WriteByte('\n')
			i = 0
		} else if i > 0 && len(line) > 0 && line[0] != ' ' && line[0] != '\t' && line[0] != '-' {
			out.WriteByte('\n')
		}
		if isPipeline && i > 1 &&
			((strings.HasPrefix(line, "  ") && len(line) > 2 && line[2] != ' ') ||
				(len(line) > 1 && line[0] == '\t' && line[1] != '\t')) {
			out.WriteString("\n")
		}
		out.WriteString(line)
		out.WriteByte('\n')
		i++
	}
	if err := scanner.Err(); err != nil {
		return nil, err
	}
	return out.Bytes(), nil
}
