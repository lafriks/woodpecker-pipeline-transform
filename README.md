# Woodpecker CI pipeline transform

[![status-badge](https://ci.codeberg.org/api/badges/lafriks/woodpecker-pipeline-transform/status.svg?branch=main)](https://ci.codeberg.org/lafriks/woodpecker-pipeline-transform)
[![Go Reference](https://pkg.go.dev/badge/codeberg.org/lafriks/woodpecker-pipeline-transform.svg)](https://pkg.go.dev/codeberg.org/lafriks/woodpecker-pipeline-transform)

Go library and utility to convert different pipelines to Woodpecker CI pipeline(s).

Currently supports converting only from Drone CI pipeline format with limited functionality.

## Drone CI pipeline supported features

* `kind` - must be `pipeline`
* `type` - only `docker` and `exec` are supported
* `name`
* `platform`
  * `os`
  * `arch`
  * `version` - not supported by Woodpecker CI
* `node`
* `trigger`
  * `branch`
  * `event`
  * `refs`
  * `repo`
  * `status`
  * `target` - not supported by Woodpecker CI
  * `cron`
  * `action` - not supported by Woodpecker CI
* `workspace`
  * `path`
* `services`
  * `name`
  * `image`
  * `pull`
  * `privileged`
  * `environment` - including `from_secret`
  * `entrypoint`
  * `commands`
  * `volumes`
    * `name`
    * `path`
* `clone`
  * `disable`
  * `depth`
* `steps`
  * `name`
  * `image`
  * `pull`
  * `settings`
  * `detach`
  * `privileged`
  * `failure` - not supported by Woodpecker CI
  * `environment` - including `from_secret`
  * `commands`
  * `volumes`
    * `name`
    * `path`
  * `when`
    * `branch`
    * `event` - `cron`, `custom`, `push`, `pull_request`, `tag` and `promote` are supported. Other events are not supported by Woodpecker CI
    * `refs`
    * `repo`
    * `instance` - only single instance condition is supported by Woodpecker CI
    * `status`
    * `target` - only single environment condition is supported by Woodpecker CI
    * `cron`
  * `depends_on` - not supported by Woodpecker CI
* `depends_on`
* `volumes`
  * `name`
  * `host`
  * `temp` - not supported by Woodpecker CI
